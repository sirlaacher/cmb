# Provide an option for testing and add the copyright test.
# Also, verify that the git LFS data has been fetched properly and
# set CMake variables that point to data directories.

option(cmb_enable_testing "Build CMB Testing" ON)
if (cmb_enable_testing)
  enable_testing()
  include(CTest)

  set(cmb_test_dir ${cmb_BINARY_DIR}/testing/temporary)
  make_directory(${cmb_test_dir})

  unset(cmb_data_dir CACHE)
  file(READ "${CMAKE_CURRENT_SOURCE_DIR}/data/cmb-data" cdata)
  if (NOT cdata STREQUAL "\n")
    message(WARNING
      "Testing is enabled, but CMB's data is not available. Use git-lfs in order "
      "to obtain the testing data.")
    set(cmb_data_dir)
  else ()
    set(cmb_data_dir "${CMAKE_CURRENT_SOURCE_DIR}/data")
  endif ()

  unset(smtk_data_dir CACHE)
  file(READ "${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/smtk/data/smtk-data" sdata)
  if (NOT sdata STREQUAL "\n")
    message(WARNING
      "Testing is enabled, but SMTK's data is not available. Use git-lfs in order "
      "to obtain the testing data.")
    set(smtk_data_dir)
  else ()
    set(smtk_data_dir "${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/smtk/data")
  endif ()

  #add the first test which is for checking the copyright
  add_test(NAME cmb-copyright-check
    COMMAND ${CMAKE_COMMAND}
      -D "cmb_SOURCE_DIR=${cmb_SOURCE_DIR}"
      -P "${CMAKE_CURRENT_SOURCE_DIR}/cmake/CheckCopyright.cmake"
  )
  set_tests_properties(cmb-copyright-check
    PROPERTIES LABELS "cmb;swprocess"
  )
endif()

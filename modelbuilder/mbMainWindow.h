//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef mbMainWindow_h
#define mbMainWindow_h

#include <QMainWindow>

/// MainWindow for the default ParaView application.
class mbMainWindow : public QMainWindow
{
  Q_OBJECT
  typedef QMainWindow Superclass;

public:
  mbMainWindow();
  ~mbMainWindow();

public slots:
  void prepare();
  void togglePostProcessingMode(bool enablePostProcessing);

protected slots:
  void showHelpForProxy(const QString& groupname, const QString& proxyname);

  void dragEnterEvent(QDragEnterEvent* evt) override;
  void dropEvent(QDropEvent* evt) override;
  void showEvent(QShowEvent* evt) override;
  void closeEvent(QCloseEvent* evt) override;

protected:
  class pqInternals;
  pqInternals* Internals;

  void testSetup();

private:
  Q_DISABLE_COPY(mbMainWindow)
};

#endif
